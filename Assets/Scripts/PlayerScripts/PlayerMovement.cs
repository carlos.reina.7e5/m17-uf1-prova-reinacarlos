using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public float Speed;
    public float _minX;
    public float _minY;
    public float _maxX;
    public float _maxY;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //transform.position = new Vector3(Speed * Input.GetAxis("Horizontal") + transform.position.x, Speed * Input.GetAxis("Vertical") + transform.position.y, transform.position.z);
        transform.position = new Vector3(Mathf.Clamp(Speed * Input.GetAxis("Horizontal") + transform.position.x, _minX, _maxX), Mathf.Clamp(Speed * Input.GetAxis("Vertical") + transform.position.y, _minY, _maxY), transform.position.z);
    }
}

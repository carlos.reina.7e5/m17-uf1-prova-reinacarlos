using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2Shooting : MonoBehaviour
{
    private float _timer = 0.0f;
    private float WaitingTime = 1.0f;
    public GameObject BulletPrefab;
    public Transform firePoint;
    public float BulletForce;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        _timer += Time.deltaTime;
        if (_timer > WaitingTime)
        {
            _timer = 0f;
            Shoot();
        }
    }

    private void Shoot()
    {
        GameObject bullet = Instantiate(BulletPrefab, firePoint.position, firePoint.rotation);
        Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
        rb.AddForce(firePoint.up * BulletForce, ForceMode2D.Impulse);
    }
}
